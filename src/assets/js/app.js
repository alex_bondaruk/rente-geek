import $ from 'jquery';
import whatInput from 'what-input';

window.jQuery = window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 47,
        nav: true,
        autoWidth:true,
        //autoplay: true,
        smartSpeed: 1000,
        autoplayTimeout: 2000,
        navText: ["<i class='chevron chevron__left'></i>","<i class='chevron chevron__right'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            768: {
                items: 2,
                nav: false
            },
            1000: {
                nav: false
            },
            1100: {
                items: 3
            }
        }
    });
});